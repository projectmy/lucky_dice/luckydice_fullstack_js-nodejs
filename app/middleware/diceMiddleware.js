const diceMiddleware = (request, response, next) => {
    console.log(`Method: ${request.method} - URL: ${request.url} - Time ${new Date()}`)

    next();
}

// EXPORT
module.exports = { diceMiddleware };