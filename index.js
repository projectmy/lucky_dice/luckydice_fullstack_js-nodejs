//  Khai báo thư viện express
const express = require('express');

// Khai báo mongo
const mongoose = require('mongoose');



// IMPORT ROUTER
const userRouter = require('./app/router/userRouter');

const diceHistoryRouter = require('./app/router/diceHistoryRouter');
const diceRouter = require('./app/router/diceRouter');

const prizeRouter = require('./app/router/prizeRouter');
const prizeHistoryRouter = require('./app/router/prizeHistoryRouter');

const voucherRouter = require('./app/router/voucherRouter');
const voucherHistoryRouter = require('./app/router/voucherHistoryRouter');

// Khai báo thư viện path
const path = require('path');

//  Khai báo app nodeJS
const app = new express();

//Khai báo middleware json
app.use(express.json());

//Khai báo middleware đọc dữ liệu UTF-8
app.use(express.urlencoded({
    extended: true
}))

//  Khai báo cổng nodeJS
const port = process.env.PORT || 8000;

// Sử dụng Mongo
mongoose.connect("mongodb://localhost:27017/CRUD_LuckyDice", (error) => {
    if (error) {
        throw error;
    }
    console.log("Connect MongoDB successfully!!!")
})


//  MIDDLEWARE
app.use((request, response, next) => {
    console.log("Time", new Date());
    next();
},
    (request, response, next) => {
        console.log("Request method: ", request.method);
        next();
    }
)


// KHAI BÁO VIEWS
app.use(express.static(path.join(__dirname + '/views')));

// Khai báo API
app.get('/', (request, response) => {
    response.sendFile(path.join(__dirname + '/views/Lucky-Dice.html'))
})


// SỬ DỤNG ROUTER
app.use('/', userRouter);
app.use('/', diceHistoryRouter);
app.use('/', diceRouter);
app.use('/', prizeRouter);
app.use('/', voucherRouter);
app.use('/', prizeHistoryRouter);
app.use('/', voucherHistoryRouter);

//  CHẠY CỔNG NODEJS
app.listen(port, () => {
    console.log(`App listening on port ${port}`);
})